<?php

namespace App\Http\Helpers;

use WebSocket\Client;
use WebSocket\ConnectionException;
use Faker\Generator as Faker;

class Helpers
{
    public static function sendRandMessage(
        string $message = null,
        string $actionName = 'message'
    ): void {
        $client = new Client(
            config('sockets.wsHost') . ':'
            . config('sockets.wsPort')
        );
        try {
            $client->send(json_encode([
                'action' => $actionName,
                'data'   => [
                    'msg' => $message ?? app(Faker::class)->text(32),
                ],
            ]));
        } catch (ConnectionException $e) {
            echo $e->getMessage();
        }
    }
}
