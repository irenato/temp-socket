<?php

namespace App\Sockets;

use App\Models\Customer;
use Illuminate\Support\Facades\DB;
use Ratchet\ConnectionInterface;
use Faker\Generator as Faker;
use stdClass;

class Socket extends BaseSocket
{
    public $clients;
    public $counter;
    private $clientsData;

    private const LIMIT_MESSAGES  = 5;
    private const ACTION_IDENTIFY = 'identify';
    private const ACTION_USERS    = 'users';
    private const FIELD_USER_NAME = 'user_name';

    public function __construct()
    {
        $this->clients     = [];
        $this->counter     = 0;
        $this->clientsData = [];
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $hostName = $conn->httpRequest->getUri()->getHost();
        if (!isset($this->clients[$hostName])) {
            $this->clients[$hostName] = [];
        }
        array_push($this->clients[$hostName], $conn);
        echo "New connection ({$conn->resourceId})" . PHP_EOL;
    }

    public function onMessage(ConnectionInterface $from, $data)
    {
        $data = json_decode($data);
        if (isset($data->data->user_name)) {
            $this->checkAction($from, $data);
        }
        $this->send($data, $from);
    }

    public function onClose(ConnectionInterface $conn)
    {
        echo "Connection ({$conn->resourceId}) has disconected" . PHP_EOL;
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $conn->close();
        echo "An error has occurred:{$e->getMessage()}" . PHP_EOL;
    }

    private function checkAction(
        ConnectionInterface $from,
        stdClass $data
    ): void {
        switch ($data->action) {
            case self::ACTION_USERS:
                if ($usersList = $this->prepareUsersList($data->data->domen)) {
                    $from->send(implode('<br>', $usersList));
                }
                break;
            case self::ACTION_IDENTIFY:
                $this->clientsData[$from->resourceId]
                          = collect($data->data)->except([
                        'page',
                        'msg',
                    ]
                );
                $userData = $this->clientsData[$from->resourceId]->toArray();
                Db::statement("INSERT INTO `customers` 
                    (
                    `" . implode('`,`', array_keys($userData)) . "`,
                    `created_at`,
                    `updated_at`
                    )
                    VALUES ('" . implode("','", $userData) . "', NOW(), NOW())
                    ON DUPLICATE KEY UPDATE
                    `updated_at` = NOW()
                    ");
            default:
                Db::statement("UPDATE `customers` 
                SET `date_message`=" . time() . ",
                count_message = count_message + 1
                WHERE 
                " . self::FIELD_USER_NAME . " = '" . $this->clientsData[$from->resourceId][self::FIELD_USER_NAME] . "'");
                break;
        }
    }

    private function prepareUsersList(string $domain): array
    {
        if (isset($domain) && isset($this->clients[$domain])) {
            $data = [];
            foreach ($this->clients[$domain] as $client) {
                if (isset($this->clientsData[$client->resourceId])) {
                    $data[$client->resourceId]
                        = $client->resourceId
                        . ' - '
                        . $this->clientsData[$client->resourceId][self::FIELD_USER_NAME];
                }
            }
            ksort($data);
            return $data;
        }
        return [];
    }

    private function send(
        stdClass $data,
        ConnectionInterface $from
    ) {
        if (isset($data->data->domen)
            && isset($this->clients[$data->data->domen])
        ) {
            $this->notify();
            foreach ($this->clients[$data->data->domen] as $client) {
                if ($from !== $client) {
                    $client->send($data->data->msg);
                }
            }
        } else {
            foreach ($this->clients as $domen => $clients) {
                foreach ($clients as $client) {
                    if ($from !== $client) {
                        $client->send($data->data->msg);
                    }
                }
            }
        }
    }

    private function notify(): void
    {
        if (++$this->counter === self::LIMIT_MESSAGES) {
            foreach ($this->clients as $domen => $clients) {
                foreach ($clients as $client) {
                    $client->send(app(Faker::class)->text());
                }
            }
            $this->counter = 0;
        }
    }
}
