<?php

namespace App\Console\Commands;

use App\Http\Helpers\Helpers;
use Illuminate\Console\Command;
use Faker\Generator as Faker;

class SendRandMessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:rand';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Helpers::sendRandMessage(app(Faker::class)->text(32));
    }
}
