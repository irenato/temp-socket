<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'user_name',
        'domen',
        'token',
        'count_message',
        'date_message',
    ];

    protected $casts = [
        'user_name' => 'string',
        'domen' => 'string',
        'token' => 'string',
        'count_message' => 'integer',
        'date_message' => 'integer',
    ];
}
