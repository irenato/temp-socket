var socket       = new WebSocket(window.custom_var.wshost),
    userName = makeRand(5),
    token = makeRand(12);
socket.onopen    = function () {
    socket.send(prepareMessage('identify'))
}
socket.onmessage = function (event) {
    $('.js-answer').append('<br>' + event.data);
};
$(document).on('submit', '.js-form', function () {
    socket.send(prepareMessage('message', $(this).find('textarea').val()))
    $(this).trigger('reset');
    return false;
})

$(document).on('click', '.js-show-users', function () {
    socket.send(prepareMessage('users'));
    return false;
})


function makeRand(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function prepareMessage(actionName, msg= '') {
    return JSON.stringify(
        {
            action: actionName,
            data  :
                {
                    domen    : document.location.host,
                    page     : document.location.pathname,
                    token    : token,
                    user_name: userName,
                    msg: msg
                }
        });
}