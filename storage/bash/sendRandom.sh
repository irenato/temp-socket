#!/bin/bash

ABSOLUTE_FILENAME=`readlink -e "$0"`
DIRECTORY=`dirname "$ABSOLUTE_FILENAME"`

if ! ps aux | grep [s]end:rand > /dev/null 2>&1
then
    php $DIRECTORY/../../artisan send:rand
fi
