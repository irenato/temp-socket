#!/bin/bash

ABSOLUTE_FILENAME=`readlink -e "$0"`
DIRECTORY=`dirname "$ABSOLUTE_FILENAME"`

if ! ps aux | grep [s]tart:ratchet > /dev/null
then
    php $DIRECTORY/../../artisan start:ratchet
fi
