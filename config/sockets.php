<?php
return [
    'wsHost' => env('WS_URL', 'ws://localhost'),
    'wsPort' => env('SOCKET_PORT', 1599)
];