<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('domen', 50)->index();
            $table->string('token', 50)->index();
            $table->string('user_name', 50)->index();
            $table->integer('date_message')->default(0);
            $table->mediumInteger('count_message')->default(0);
            $table->timestamps();
            $table->unique('token');
            $table->unique('user_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
